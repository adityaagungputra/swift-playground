//: Playground - noun: a place where people can play

import UIKit

func printName(_ name: String) -> String {
    return "My name is \(name)"
}

func printName2(_ name: String){
    print("My name is \(name)")
}

func factorial(number: Int) -> Int{
    var result: Int = 1
    if (number > 0){
        for index in 1 ... number {
            result *= index
        }
        return result
    } else {
        return 1
    }
}

func power(number: Int, pow: Int = 2) -> Int {
    var result: Int = 1
    for _ in 1 ... pow{
        result *= number
    }
    return result
}

func swap(_ a: inout Int, _b: inout Int){
    let temp = a
    a = _b
    _b = temp
}

//generic function
func swapGeneric<T> (_ a: inout T, _ b: inout T){
    let temp = a
    a = b
    b = temp
}

print(printName("Budiman"))
print(factorial(number: 9))
print(power(number: 4, pow: 5))
print(power(number: 5))
printName2("Tukul")

var a = 10
var b = 20
swap(&a, &b)
print("a: \(a), b: \(b)")

var c = "Calculus"
var d = "Sport"
swapGeneric(&c, &d)
print("c: \(c), d: \(d)")
