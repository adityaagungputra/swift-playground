//: Playground - noun: a place where people can play

import UIKit

//ARRAY
let numberArray = [1, 2, 3]

//array declaration with let cause the array to be immutable, i.e. the contents are unchangeable
//insertion or deletion of an element are impossible too
//numberArray.append(contentsOf: [4, 5])

var numberList = [Int]()
numberList.append(contentsOf: [2, 3, 4, 7]) //append
numberList += [9, 12, 10] //array concatenation
for number in numberList{
    print(number)
}
for (index, element) in numberList.enumerated(){
    print("Number \(index + 1): \(element)")
}

var marks = Array(repeating: 0.0, count: 5)
for mark in marks {
    print(mark)
}
print(marks.isEmpty) //false
print(marks.capacity) //6
print(marks.count) //5

var stack = [String]()
stack.insert("First", at: 0)
stack.insert("Second", at: 0)
stack.insert("Third", at: 0)
for word in stack {
    print(word)
}
var pop = stack.removeFirst()
print("Popped element: \(pop)")
print("Remaining element: \(stack.count)")

var queue = [(Int, Int)]()
queue.append((2,3))
queue.append((4,5))
queue.append((8,9))
for (x, y) in queue{
    print("(\(x), \(y))")
}
var dequeued = queue.removeFirst()
print("Dequeued point: (\(dequeued.0), \(dequeued.1))")

//SET
var studentsA = Set<String>()
studentsA.insert("Andy")
studentsA.insert("Ben")
studentsA.insert("Carol")

for name in studentsA.sorted() {
    print(name)
}

if studentsA.contains("David"){
    print("The set contains David")
}

var studentsB: Set<String> = ["Ben", "Ed", "Fifi"]

var intsc = studentsA.intersection(studentsB)
for name in intsc{
    print(name)
}

//DICTIONARY
var stock = [String:Int]()
stock["Jelly"] = 9
stock["Plate"] = 22
for (item, quantity)in stock{
    print("Item: \(item), Quantity: \(quantity)")
}
if let plateQty = stock["Plate"]{
    print("Amount of plates: \(plateQty)")
} else {
    print("We have no plate")
}

var bio = [String:Any]()
bio["Age"] = 55
bio["Name"] = "Ben"
bio["City"] = "Tokyo"
for values in bio.values{
    print(values)
}
bio["Age"] = 23
print(bio["Age"]!)
bio["City"] = nil //same as bio.removeValue()
for values in bio.values{
    print(values)
}

//FOR LOOPING
for index in 0..<9{
    print(index)
}
