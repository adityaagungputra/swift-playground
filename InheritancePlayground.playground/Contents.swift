//: Playground - noun: a place where people can play

import UIKit

//Vehicle study case
class Vehicle {
    
    var driver: String
    var horn: String?
    
    init(driver: String){
        self.driver = driver
    }
    
    convenience init() {
        self.init(driver: "")
    }
    
    func makeHorn(){
        if let horn = horn {
            print(horn)
        } else {
            print("Don't have horn voice")
        }
    }
}

class Bus: Vehicle {
    
    var capacity: Int?
    
    override init(driver: String){
        capacity = 0
        super.init(driver: driver)
    }
    
    override func makeHorn() {
        print("Telolet telolet")
    }
}

class Car: Vehicle {
    override init(driver: String){
        super.init(driver: driver)
    }
}

print("================VEHICLE================")
var becak = Vehicle()
becak.horn = "Tring tring"
becak.makeHorn()

var bis = Bus()
bis.makeHorn()

var parkingLot = [
    Bus(driver: "Anthony"),
    Car(driver: "Boy"),
    Bus(driver: "Cindy"),
    Car(driver: "Danny"),
    Car(driver: "Eric")
]

var nBus = 0
var nCar = 0
for vehicle in parkingLot {
    if vehicle is Bus {
        nBus += 1
    } else if vehicle is Car {
        nCar += 1
    }
}
print("Number of bus is \(nBus) meanwhile number of car is \(nCar)")

for vehicle in parkingLot {
    if let bus = vehicle as? Bus {
        print("The bus driver is \(bus.driver)")
    } else if let car = vehicle as? Car {
        print("The car belongs to \(car.driver)")
    }
}

//Animal study case
class Animal {
    var name: String?
    
    init(){}
    
    //failable initialization, prevent invalid parameter
    init?(name: String){
        if name.isEmpty {
            return nil
        }
        self.name = name
    }
}

class Mammal: Animal {
    override init(name: String){
        super.init()
        if name.isEmpty {
            self.name = "Unnamed Mammal"
        } else {
            self.name = name
        }
    }
    
    convenience override init() {
        self.init(name: "")
    }
}

extension Mammal {
    
    func giveBirth() -> Mammal {
        return Mammal()
    }
}

print("\n================ANIMAL================")
var cat = Animal(name: "Meong")
if let cat = cat {
    print("Cat initialized")
} else {
    print("Cat initialization failed")
}

var dolphin = Mammal()
print(dolphin.name!) //print "Unnamed Mammal"
var babyDolphin = dolphin.giveBirth()
babyDolphin.name = "Junior"

//Extension study case
extension Int {
    
    enum Kind{
        case negative, zero, positive
    }
    
    var kind: Kind {
        switch self {
        case 0:
            return .zero
        case let x where x > 0:
            return .positive
        default:
            return .negative
        }
    }
    
    mutating func inverse(){
        self = 0 - self
    }
}

print("\n================EXTENSION================")
var number = 9
print(number.kind)
number.inverse()
print("Number now is \(number)")