//: Playground - noun: a place where people can play

import UIKit

//Contains operator overloading
class ComplexNumber {
    
    var real: Int
    var img: Int
    
    init(real: Int, img: Int) {
        self.real = real
        self.img = img
    }
    
    static func + (left: ComplexNumber, right: ComplexNumber) -> ComplexNumber {
        return ComplexNumber(real: left.real + right.real, img: left.img + right.img)
    }
    
    static func - (left: ComplexNumber, right: ComplexNumber) -> ComplexNumber {
        return ComplexNumber(real: left.real - right.real, img: left.img - right.img)
    }
    
    static func += (left: inout ComplexNumber, right: ComplexNumber){
        left = left + right
    }
}

var cn1 = ComplexNumber(real: 2, img: 3)
var cn2 = ComplexNumber(real: 3, img: 1)

let cnPlus = cn1 + cn2
let cnMinus = cn1 - cn2
print("Addition result is \(cnPlus.real) + \(cnPlus.img)i")
print("Subtraction result is \(cnMinus.real) + \(cnMinus.img)i")
cn1 += cn2
print("Number #1 is now \(cn1.real) + \(cn1.img)i")

//Custom operator in a generic class
infix operator << : AdditionPrecedence
postfix operator >>

class Stack<Element>{
    
    var array = [Element]()
    var size = 0
    
    func push(_ e : Element){
        array.insert(e, at: 0)
        size += 1
    }
    
    func pop() -> Element? {
        if size > 0 {
            let e = array.remove(at: 0)
            size -= 1
            return e
        }
        return nil
    }
    
    //create << operator
    static func << (_ stack: Stack, _ e: Element){
        stack.push(e)
    }
    
    //define >>
    static postfix func >> (_ stack: inout Stack) -> Element? {
        return stack.pop()
    }
}

var numbers = Stack<Int>()
numbers.push(22)
numbers << 33
numbers << 44 //now the stack has 3 elements

for int in numbers.array {
    print(int)
}

let pop = numbers>>
print(pop!) //will print 44