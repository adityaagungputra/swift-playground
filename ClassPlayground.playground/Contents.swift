//: Playground - noun: a place where people can play

import UIKit

//example of enum, struct and class

enum Direction {
    case north, east, south, west
}

enum Barcode {
    case upc(Int, Int, Int, Int)
    case qrCode(String)
}

struct Point3D{
    var x: Double = 0.0
    var y: Double!
    var z: Double = 0.0
    var distanceToOrigin: Double {
        return sqrt(x*x + y*y + z*z)
    }
    mutating func toRight(){
        x += 1
    }
}

class Zombie{
    var name: String? {
        willSet(newName){
            print("New zombie name is \(newName!)")
        }
        didSet {
            if let oldValue = oldValue {
                print("Name was \(oldValue) and now is \(name!)")
            }
        }
    }
    var parent: Zombie?
    var move: Direction
    var victim = [String]()
    
    init(){
        self.name = ""
        self.parent = nil
        self.move = .north
    }
    
    subscript(index: Int) -> String {
        get {
            assert(index < victim.count, "No such victim")
            return victim[index]
        }
        set {
            assert(index <= victim.count, "Cannot set victim")
            if (index == victim.count){
                victim += [newValue]
            } else {
                victim[index] = newValue
            }
        }
    }
}

class User{
    var firstName: String!
    var lastName: String!
    
    //computed properties
    var fullName: String! {
        get{
            return firstName + " " + lastName
        }
        set{
            if let spaceIdx = newValue.characters.index(of: " "){
                firstName = newValue.substring(to: spaceIdx)
                var lastTemp = newValue.substring(from: spaceIdx)
                lastTemp.remove(at: lastTemp.startIndex)
                lastName = lastTemp
            } else {
                firstName = newValue
                lastName = ""
            }
        }
    }
}

print("======================ENUM======================")
var barcode = Barcode.qrCode("abcde")
var barcode2 = Barcode.upc(2, 3, 4, 5)
print(barcode) //print qrCode("abcde")
print(barcode2) //print upc(2, 3, 4, 5)

print("\n=================STRUCT TEST===================")
var p1 = Point3D(x: 0.0, y: 2.0, z: 3.0)
var p2 = Point3D()
//print(p2.y)
print(p1.distanceToOrigin)
p1.toRight()
print("Now x = \(p1.x)")

print("\n=================CLASS TEST==================")
var child = Zombie()
child.name = "Ronnie" //will print willSet and didSet method
child.victim.append("Alan")
print(child[0])
child[1] = "Benny"
child[0] = "Alicia"
for victim in child.victim{
    print("Victim name: \(victim)")
}

var user = User()
user.firstName = "Donald"
user.lastName = "Trump"
print(user.fullName)
user.fullName = "Joko Widodo"
print(user.fullName)
print("Now first name is \(user.firstName!)")
print("Now last name is \(user.lastName!)")
