//: Playground - noun: a place where people can play

import UIKit

//numeric types
print("=======================NUMBER====================")
let nine: Int = 9
var eight: Int!
//eight = nil
eight = 8
print(nine + 10)
print(sqrt(Double(eight)))
print(eight << 2)
print(eight)

if let eight = eight{
    print(eight >> 2)
}

//boolean
print("\n=======================BOOLEAN====================")
var sick: Bool = true
if sick {
    print("Sick")
} else {
    print("Healthy")
}

//string
print("\n======================STRING=====================")
var name: String = "Budi"
print("My name is \(name)")
for character in name.characters{
    print(character)
}
print(name.characters.count)
print(name.uppercased())
print(name.hasPrefix("Bu"))
name.insert(contentsOf: "ono".characters, at: name.endIndex)
print(name)
let string = "Hello.World."
let dot: Character = "."
if let idx = string.characters.index(of: dot) {
    let pos = string.characters.distance(from: string.startIndex, to: idx)
    print("Found \(dot) at position \(pos)")
}
else {
    print("Not found")
}

//Tuples
print("\n======================TUPLES=====================")
let item = ("Nasi Uduk", "Indomaret", 3000)
let (food, supplier, price) = item
print("Food name: \(item.0)")
print(supplier) //Indomaret

//Optional
print("\n======================OPTIONAL=====================")
var optNumber: Int? //default value is nil
optNumber = 9
print(optNumber) //will print Optional(9)
print(optNumber!) //exclamation mark is needed to unwrap the optional value

var optNumber2: Int! = 10 //implicitly unwrapped optional
print(optNumber2) //will print 10, no need ! mark

let numberString = "223"
var parsedInt = Int(numberString) //this results optional variable
print("Parsed number: \(parsedInt!)")

//optional binding, check whether number is nil or not
if let number = Int(numberString){
    print(number)
}

var myname: String! = "Joko"
print(myname)
