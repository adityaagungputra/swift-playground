//: Playground - noun: a place where people can play

import UIKit

protocol Shape {
    
    init()
    
    func Area() -> Double
}

protocol NamedObject {
    func getName() -> String
}

class Rectangle: Shape {
    
    var length: Double
    var width: Double
    
    required init() {
        length = 0.0
        width = 0.0
    }
    
    init(length: Double, width: Double){
        self.length = length
        self.width = width
    }
    
    func Area() -> Double {
        return length * width
    }
}

class Square: Shape, NamedObject {

    var side: Double
    var name: String?

    required init(){
        side = 0.0
    }
    
    init(side: Double){
        self.side = side
    }
    
    init(side: Double, name: String) {
        self.side = side
        self.name = name
    }
    
    func Area() -> Double {
        return side * side
    }
    
    func getName() -> String {
        if let name = self.name {
            return name
        }
        return ""
    }
}

var drawing = [AnyObject]()
drawing.append(Rectangle(length: 2.0, width: 3.0))
drawing.append(Square(side: 4.0))

for item in drawing {
    if let item = item as? Shape {
        print(item.Area())
    }
}
