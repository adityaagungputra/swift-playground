//: Playground - noun: a place where people can play

import UIKit

//LOOPING
for index in 1...10 {
    print(index)
}

for index in 2..<5{
    print(index)
}

var start: Int = 0
var inc: Int = 1
repeat {
    start += inc
    inc += 2
    if (start < 100){
        print(start)
    }
} while (start < 100)


//SWITCH
var name = "Budi"
switch name[name.startIndex]{
case "A", "I", "U", "E", "O":
    print("Vowel")
default:
    print("Consonant")
}

let age = 12 //next Teenager will be printed
switch age {
case 1..<12:
    print("Children")
case 12..<17:
    print("Teenager")
case 17..<25:
    print("Young")
case 25..<65:
    print("Adult")
default:
    print("Retired")
}

let point = (3, -3)
switch point{
case let (x, y) where x == y:
    print("On the line x = y")
case let (x, y) where x == -y:
    print("On the line x = -y")
default:
    print("I don't know where")
}

//EARLY EXIT (GUARD)
var student: String?
func printName(name: String?){
    guard let name = name else {
        print("No name")
        return
    }
    print("My name is \(name)")
}

printName(name: student)
student = "Arida"
printName(name: student)
